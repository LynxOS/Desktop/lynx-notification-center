var mute;
var vol;

async function initVol(){
  vol = await loudness.getVolume();
  mute = await loudness.getMuted();
  drowVolumen();
  drowMute();
}

async function toggleMute(){
  await loudness.setMuted(!mute);
  mute = !mute;
  drowMute();
}

async function setVolumen(){
  intVol = document.getElementById('volumenData').value
  await loudness.setVolume(intVol);
  vol = intVol
  drowMute();
}

async function drowVolumen(){
  document.getElementById('volumen').innerHTML = `
    <input
      type="range"
      class="form-range"
      min="0"
      max="100"
      value="${vol}"
      onchange="setVolumen();"
      id="volumenData" />
  `;
  document.getElementById('volumenData').value = vol;
}

async function drowMute(){
  let icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'audio-volume-muted-blocking-symbolic']).stdout.toString();
  if (mute){
    icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'audio-volume-muted-symbolic']).stdout.toString();
  }else{
    if (vol > 66){
      icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'audio-volume-high-symbolic']).stdout.toString();
    } else if (vol > 33){
      icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'audio-volume-medium-symbolic']).stdout.toString();
    } else {
      icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'audio-volume-low-symbolic']).stdout.toString();
    }
  }

  document.getElementById('mute').innerHTML = `
    <button
      class="btn btn-primary"
      onclick="toggleMute()">
      <img src="file://${icon}" class="img-mute" />
    </button>
  `;
}