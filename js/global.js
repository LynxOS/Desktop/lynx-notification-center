const homePath = nw.App.dataPath.concat('/../../../');
const win = nw.Window.get();
const loudness = require('loudness');
const execSynx = require('child_process').spawnSync;

// Set windows Properties
win.setShowInTaskbar(false);
win.resizeTo(400, Math.round(screen.height - 80));
win.moveBy(Math.round(screen.width - 400), 20);
win.setResizable(false);
win.focus();
win.on('blur', function (evt) {
  win.close();
});

async function setArea(){
  document.getElementById('notification-center').style.height = Math.round(screen.height - 160).toString().concat('px');
  document.getElementById('notification-area').style.height = Math.round(screen.height - 280).toString().concat('px');
  document.getElementById('status-area').style.height = '80px'; 
}
